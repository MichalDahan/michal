import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from urllib.request import urlopen
from bs4 import BeautifulSoup
import ssl

def scraper(url):
    context = ssl._create_unverified_context()
    html = urlopen(url, context=context)
    soup = BeautifulSoup(html, 'html.parser')
    table=soup.find('div', class_='bsi_table') #find the table that includes the business info
    res={}
    for tr in table.findAll('div'): #foreach element in the table take the label to be the key and the value to be the value inside the collection
        labal= tr.find('span', {'class': 'bsi_cell_label'})
        value= tr.find('span', {'class':'bsi_cell_value'})
        txtlabal=labal.text[:-1]
        txtvalue=value.text
        res[txtlabal]= txtvalue
    return res


url = "https://www.ebay.com/usr/shoesparadise.ita"
print(scraper(url))
