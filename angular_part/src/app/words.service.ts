import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class WordsService {

  apiURL: string= 'http://localhost:8080/word/';
  constructor(private http: HttpClient) { }

  insertNewWord(bodyData)
  {
    return this.http.post(this.apiURL+'insert', bodyData, {headers:{'Content-Type':'application/json'}});
  }

  getMatch(query)
  {
    return this.http.get(this.apiURL+'match?query=', {headers:{'Content-Type':'application/json'}, params:{'query': query}});
  }
}