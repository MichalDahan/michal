import { Component, OnInit } from '@angular/core';
import { WordsService } from '../app/words.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'wordsProj';
  constructor(private data: WordsService) { }
  model = {};
  response= {err:false, success: false, message:''};
  ngOnInit() {
  }

  insert()
  {
    this.data.insertNewWord(this.model)
    .subscribe(data=>{
      this.response.err=true; this.response.success=true; this.response.err=false; this.response.message='New word inserted :)';
      console.log('data:', data)
    },
    (error) => {console.log('Oops', error); this.response.err=true; this.response.success=false; this.response.message=error.error.message})
  }

  match()
  {
    this.data.getMatch(this.model)
    .subscribe(data=>{
      this.response.err=true; this.response.success=true; this.response.err=false; this.response.message='The top matches: '+data['top'];
      console.log('data:', data)
    },
    (error) =>{console.log(error)}) //{console.log('Oops', error); this.response.err=true; this.response.success=false; this.response.message=error.error.message})
  }

}
