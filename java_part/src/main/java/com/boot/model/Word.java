package com.boot.model;

import java.util.Comparator;

public class Word implements Comparable<Word> {
	public String word;
	public int score=0;
	
	public Word(String word)
	{
		this.word=word;
        char[] asciiStr = word.toCharArray();
        for(char ch:asciiStr){
        	if (Character.isLetter(ch))
        		this.score+=(int)ch;
        }
	}
	
	public Word(String word, int score)
	{
		this.word=word;
        this.score=score;
	}
	
	public int getScore()
	{
		return this.score;
	}
	
	 public int compareTo(Word word)
	 {
		 if(this.score<word.score)
			 return -1;
		 if(this.score>word.score)
			 return 1;
		 return 0;
	 }
	 
	    @Override
	    public String toString() { 
	        return "word: "+word+" score: "+score; 
	    }

	    @Override
	    public boolean equals(Object o)
	    {
	    	Word w=(Word)o;
	    	if(this.word==w.word)
	    		return true;
	    	return false;
	    }
}
