package com.boot.model;

import java.util.ArrayList;
import java.util.TreeSet;

public class WordsList {
	public static ArrayList<Word> wordsList = new ArrayList<Word>();
	
	public static void addWord(String word)
	{
		Word w= new Word(word);
		if(!(wordsList.contains(w)))
			wordsList.add(w);
		System.out.println(wordsList);
	}
	
	public static ArrayList<String>match(String word)
	{
		ArrayList<String> resList= new ArrayList<String>();
		TreeSet<Word> words=new TreeSet<Word>();
		Word w= new Word(word);
		for(Word wo: wordsList)
		{
			words.add(new Word(wo.word, Math.abs(wo.score-w.score)));
		}
		int resSize= words.size()<3? words.size():3;
		for(int i=0;i<resSize;i++)
		{
			resList.add(words.pollFirst().word);
		}
		return resList;
		
	}
	
    @Override
    public String toString() { 
        String str="";
        for(Word w: wordsList)
        {
        	str+=w.toString();
        	str+="\n";
        }
        return str;
    } 

}
