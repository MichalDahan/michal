package com.boot.model;

import java.util.ArrayList;

public class MatchResponse {
	public String query;
	public ArrayList<String> top;
	
	public MatchResponse(String query, ArrayList<String> top)
	{
		this.query= query;
		this.top= top;
	}
}
