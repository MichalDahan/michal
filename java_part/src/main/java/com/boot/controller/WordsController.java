package com.boot.controller;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.boot.model.MatchResponse;
import com.boot.model.WordsList;

@RestController
@RequestMapping("word/")
public class WordsController {
	
	@SuppressWarnings("finally")
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="insert", method=RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> tryd(@RequestBody Map<String, String> payload) {
		int err=0;
		boolean success= true;
		try
		{
		String query= payload.get("word");		
        WordsList.addWord(query);
		}
		catch(Exception e)
		{
			Random ran = new Random();
			err= ran.nextInt(101)+400; //rand error # between 400 and 500
			success=false;
		}
		finally
		{
			return new ResponseEntity<>("{\"success\":\""+success+"\", \"err\":\""+err+"\"}",HttpStatus.OK);
		}
	}
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="match", method=RequestMethod.GET, produces = "application/json")
	public MatchResponse match(@RequestParam("query") String query) {
		ArrayList<String> mWords= WordsList.match(query);
		return new MatchResponse(query, mWords);
	}

}
